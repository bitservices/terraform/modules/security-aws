<!---------------------------------------------------------------------------->

# kms/key

#### Manage [KMS] encryption keys

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/security/aws//kms/key`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_kms_key" {
  source  = "gitlab.com/bitservices/security/aws//kms/key"
  owner   = "terraform@bitservices.io"
  name    = "foo-bar"
  company = "BITServices Ltd"
}
```

<!---------------------------------------------------------------------------->

[KMS]: https://aws.amazon.com/kms

<!---------------------------------------------------------------------------->
