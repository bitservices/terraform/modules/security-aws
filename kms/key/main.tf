###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of this KMS key."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

###############################################################################
# Optional Variables
###############################################################################

variable "enabled" {
  type        = bool
  default     = true
  description = "Specifies whether the KMS key is enabled."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "The description of the KMS key as viewed in AWS console."
}

###############################################################################

variable "key_usage" {
  type        = string
  default     = "ENCRYPT_DECRYPT"
  description = "Specifies the intended use of the key."
}

variable "key_policy" {
  type        = string
  default     = null
  description = "A valid policy JSON document."
}

variable "key_rotation" {
  type        = bool
  default     = false
  description = "Specifies whether key rotation is enabled."
}

variable "key_deletion_days" {
  type        = number
  default     = 7
  description = "Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days."
}

###############################################################################
# Locals
###############################################################################

locals {
  alias = format("alias/%s", var.name)
}

###############################################################################
# Resources
###############################################################################

resource "aws_kms_key" "scope" {
  policy                  = var.key_policy
  key_usage               = var.key_usage
  is_enabled              = var.enabled
  description             = var.description
  enable_key_rotation     = var.key_rotation
  deletion_window_in_days = var.key_deletion_days

  tags = {
    Name    = var.name
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

###############################################################################

resource "aws_kms_alias" "scope" {
  name          = local.alias
  target_key_id = aws_kms_key.scope.key_id
}

###############################################################################
# Outputs
###############################################################################

output "name" {
  value = var.name
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "alias" {
  value = local.alias
}

output "enabled" {
  value = var.enabled
}

output "description" {
  value = var.description
}

###############################################################################

output "key_usage" {
  value = var.key_usage
}

output "key_policy" {
  value = var.key_policy
}

output "key_rotation" {
  value = var.key_rotation
}

output "key_deletion_days" {
  value = var.key_deletion_days
}

###############################################################################

output "id" {
  value = aws_kms_key.scope.key_id
}

output "arn" {
  value = aws_kms_key.scope.arn
}

###############################################################################

output "alias_arn" {
  value = aws_kms_alias.scope.arn
}

###############################################################################
